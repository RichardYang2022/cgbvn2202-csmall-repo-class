package cn.tedu.mall.front.service.impl;

import cn.tedu.mall.common.exception.CoolSharkServiceException;
import cn.tedu.mall.common.restful.ResponseCode;
import cn.tedu.mall.front.service.IFrontCategoryService;
import cn.tedu.mall.pojo.front.entity.FrontCategoryEntity;
import cn.tedu.mall.pojo.front.vo.FrontCategoryTreeVO;
import cn.tedu.mall.pojo.product.vo.CategoryStandardVO;
import cn.tedu.mall.product.service.front.IForFrontCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@DubboService
@Service
@Slf4j  //提供log对象
public class FrontCategoryServiceImpl implements IFrontCategoryService {
    //开发规范标准：为了降低Redis中的key拼写错误的风险，我们都会定义常量
    public static final String CATEGORY_TREE_KEY = "category_tree";
    //当前模块查询所有分类信息要依赖product模块，所有需要dubbo调用product模块查询pms库中的
    //pms_category表中的数据
    @DubboReference
    private IForFrontCategoryService dubboCategoryService;
    //将查询出的结果保存到redis中，以备后续用户调用
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public FrontCategoryTreeVO categoryTree() {
        //我们会将查询的三级分类树保存在redis中，所以先检查redis中是否包含上面定义的key
        if(redisTemplate.hasKey(CATEGORY_TREE_KEY)){
            //如果判断redis中已经包含分类树信息，直接从redis中获取返回即可
            FrontCategoryTreeVO<FrontCategoryEntity> treeVO =
                    (FrontCategoryTreeVO<FrontCategoryEntity>)
                            redisTemplate.boundValueOps(CATEGORY_TREE_KEY).get();
            return treeVO;
        }
        //Redis中没有三级分类树信息，当前请求是第一个运行该方法的请求
        //第一次运行该方法的请求。应该去数据库查询----利用dubbo调用product模块去查询所有分类信息对象
        List<CategoryStandardVO> categoryStandardVOS = dubboCategoryService.getCategoryList();
        //将查询出来的所有的分类信息对象转成三级分类树---单独写成一个方法：initTree()
        FrontCategoryTreeVO<FrontCategoryEntity> treeVo = initTree(categoryStandardVOS);
        //将转换完成的treeVo保存到Redis中，以备后续使用
        redisTemplate.boundValueOps(CATEGORY_TREE_KEY).set(treeVo,24, TimeUnit.HOURS);
        //千万别忘记返回
        return treeVo;
    }
    //构建三级分类树
    private FrontCategoryTreeVO<FrontCategoryEntity> initTree(List<CategoryStandardVO> categoryStandardVOS) {
        //第一部分：确定所有分类对象的父分类
        //声明一个Map，使用父分类id做这个map的key，使用当前分类对象集合对应这个map的value
        //将所有相同父分类的对象添加到正确的集合中
        Map<Long,List<FrontCategoryEntity>> map = new HashMap<>();
        log.info("当前分类对象总数为：{}",categoryStandardVOS.size());
        //变量categoryStandardVOS，进行下一步
        for (CategoryStandardVO categoryStandardVO : categoryStandardVOS) {
            //CategoryStandardVO对象没有children属性，不能保存子分类
            //所以我们先将它转换为能保存子分类的FrontCategoryEntity
            FrontCategoryEntity frontCategoryEntity = new FrontCategoryEntity();
            BeanUtils.copyProperties(categoryStandardVO,frontCategoryEntity);
            //因为后面要反复使用当前分类对象的父分类id所以取出
            Long parentId = frontCategoryEntity.getParentId();
            //根据当前分类对象的父分类id向map添加元素，但是先判断是否已经存在这个key
            if(map.containsKey(parentId)){
                //如果有这个key，我们直接将当前分类对象添加到map的value的List中
                map.get(parentId).add(frontCategoryEntity);
            }else {
                //当前map没有这个key
                //我们要创建List对象，将分类对象保存在这个List中
                List<FrontCategoryEntity> value =  new ArrayList<>();
                value.add(frontCategoryEntity);
                //使用当前的parentId做key，上面的value保存到key中
                map.put(parentId,value);
            }
        }
        //第二部分：将子分类对象关联到父分类对象的children属性中
        //第一部分中获得Map已经包含了所有父分类包含的子分类对象
        //下面我们就可以从根分类开始，通过循环遍历每个级别的分类对象添加到对应级别的children属性中
        //一级分类的父id在数据库中设计为0，是已知的，所有代码直接写死获得父分类id为0所有的子分类
        List<FrontCategoryEntity> firstLevels = map.get(0L);
        //判断firstLevels是否为空
        if(firstLevels==null)
            throw new CoolSharkServiceException(ResponseCode.INTERNAL_SERVER_ERROR,"当前项目没有根分类");
        //遍历所有一级分类对象
        for (FrontCategoryEntity firstLevel : firstLevels) {
            //获取当前一级分类的id(是二级分类的父id)
            Long secondLevelParentId = firstLevel.getId();
            //获得当前分类对象的所有子分类(获取二级分类集合)
            List<FrontCategoryEntity> secondLevels = map.get(secondLevelParentId);
            //判断是否包含二级分类对象
            if(secondLevels==null){
                log.warn("当前分类缺少二级分类内容:{}",secondLevelParentId);
                continue;
            }
            //遍历二级分类集合
            for (FrontCategoryEntity secondLevel : secondLevels) {
                //获取当前二级分类对象的id(三级分类对象的父id)
                Long thirdLevelParentId = secondLevel.getId();
                List<FrontCategoryEntity> thirdLevels = map.get(thirdLevelParentId);
                if(thirdLevels==null){
                    log.warn("当前分类缺少三级分类内容：{}",secondLevelParentId);
                    continue;
                }
                //将三级分类集合添加到二级分类对象的children属性中
                secondLevel.setChildrens(thirdLevels);
            }
            //将二级分类对象集合添加到一级分类对象的children属性中
            firstLevel.setChildrens(secondLevels);
        }
        //到此为止，所有分类对象的父子关系已经构建完成
        //但是本方法的返回值类型是FrontCategoryTreeVO<FrontCategoryEntity>，还没有创建
        FrontCategoryTreeVO<FrontCategoryEntity> treeVO = new FrontCategoryTreeVO<>();
        //将一级分类的集合赋值给该对象
        treeVO.setCategories(firstLevels);
        return treeVO;
    }
}
